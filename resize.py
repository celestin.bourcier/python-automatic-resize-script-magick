import os, time, sys, subprocess 
from PIL import Image

progressBarCallback =None

def set_callbacks(_progressBarCallback = None, _actualFileCallback = None, _actualFileIndexCallback = None):
    global progressBarCallback
    global actualFileCallback
    global actualFileIndexCallback

    # Vérification de l'existence des callbacks avant leur sauvegarde 
    if (_progressBarCallback != None):
        progressBarCallback = _progressBarCallback
    
    if (_actualFileCallback != None):
        actualFileCallback = _actualFileCallback

    if (_actualFileIndexCallback != None):
        actualFileIndexCallback = _actualFileIndexCallback

def update_progress(progress):
    # Appelle la callback afin de mettre à jour l'avancement sur l'UI
    if (progressBarCallback != None):
        progressBarCallback(int(progress*100))

def displayActualFile(actualFilePath : str):
    # Appelle la callback afin de mettre à jour le fichier en conversion sur l'UI
    if (actualFileCallback != None):
        actualFileCallback(actualFilePath)

def displayActualIndex(value : int):
    # Appelle la callback afin de mettre à jour l'index de conversion sur l'UI
    if (actualFileIndexCallback != None):
        actualFileIndexCallback(value)

def countAllFilesRecursiveJPG(rep):
    nbFile = 0
    
    # Compte chaque fichier JPG
    for root, dirs, files in os.walk(rep):
        for name in files:
            if name.endswith((".JPG", ".jpeg", ".jpg", ".JPEG")):
                nbFile+= 1

    return nbFile

def convertAllFilesRecusiveJPG(rep, destPath, max_width : int, max_height : int , final_dpi : int, folderCopy : bool):
    nbFile = countAllFilesRecursiveJPG(rep)
    i = 0
    out = sys.stdout
    addToPath = ""

    # Boucle sur l'ensemble des fichiers présents dans le répertoire sélectionenr 
    for root, dirs, files in os.walk(rep):
        for name in files:
            if name.endswith((".JPG", ".jpeg", ".jpg", ".JPEG")):
                image_location = (root+'\\' +name).replace("\\\\", "\\")

                if (folderCopy):
                    addToPath = str(root).replace(rep, "")
                    
                    # Créer l'arborescence de dossier (si le dossier existe déjà on passe)
                    try : 
                        os.makedirs(destPath + addToPath)
                    except OSError :
                        pass

                with Image.open(image_location) as img:
                    i+=1

                    displayActualIndex(i)
                    displayActualFile(image_location)

                    # Obtenir la taille de l'image
                    width, height = img.size

                    # Calculer le ratio afin de garder les proportions de l'image 
                    img_ratio = width / height

                    # Calcul de la nouvelle taille de l'image
                    new_size = (0, 0)

                    # Cas d'une image horizontale 
                    if (img_ratio > 1):
                        if (width > max_width) or (height > max_height):
                            new_size = (max_width, int(max_width / img_ratio))

                    # Cas d'une image carrée 
                    elif (img_ratio == 1):
                        if (width > max_width) or (height > max_width):
                            new_size = (max_width, max_width)

                    # Cas d'une image verticale 
                    else:
                        if (width > max_height) or (height > max_width):
                            new_size = (int(max_width / img_ratio), max_width)

                    if (new_size != (0, 0)):
                        img_resized = img.resize(new_size)
                    else: 
                        img_resized = img

                    # Sauvegarder l'image redimensionnée
                    img_resized.save(destPath + addToPath + "\\" + name, dpi = (final_dpi, final_dpi), exif=(img.info.get('exif')))

                    update_progress(i/nbFile)


def repAndSubfoldersConversion(sourcePath, destPath, width, height, dpi, folderCopy : bool):
    
    print("Count files that will need a conversion")
    nbFileToConvert = countAllFilesRecursiveJPG(sourcePath)
    print(str(nbFileToConvert) + " files to convert")

    start = time.time()

    convertAllFilesRecusiveJPG(sourcePath, destPath, width, height, dpi, folderCopy)

    end = time.time()

    print("Process duration   : " + str(end - start) + " sec.")
    if (end-start != float(0)):
        if (nbFileToConvert != 0):
            print("Duration per image : " + str((end-start)/float(nbFileToConvert)) + " sec.")
