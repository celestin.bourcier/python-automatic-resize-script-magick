
import tkinter as tk
from tkinter import filedialog, ttk
import resize
import threading

def select_source_directory():
    global file_counted_sv
    directory = filedialog.askdirectory()
    if directory:
        source_directory.set(directory)
        file_counted_sv.set("File number : " + str(resize.countAllFilesRecursiveJPG(directory)))

def select_destination_directory():
    directory = filedialog.askdirectory()
    if directory:
        destination_directory.set(directory)

def start_progress():
    resize.set_callbacks(progress_bar_update, actual_file_name_update, actual_file_index_update)
    src = source_directory.get()
    dst = destination_directory.get()
    if ((src) and (dst)):
        src = src.replace("/","\\")
        dst = dst.replace("/","\\")

        #src = "\"" + src + "\""
        #dst = "\"" + dst + "\""

        threading.Thread(target=resize.repAndSubfoldersConversion, args=(src, dst, int(width_entry.get()), int(height_entry.get()), int(dpi_entry.get()), directory_reproduction_var.get())).start()

def progress_bar_update(value : int):
    if ((value <= 100) and (value >= 0)):
        progress_var.set(value)

def actual_file_name_update(value : str):
    actual_file_sv.set(value)

def actual_file_index_update(value : int):
    file_index_sv.set(value)

def validate_number(P):
    if P.isdigit():
        return True
    return False

# Création de la fenêtre principale
root = tk.Tk()
root.title("Picture size modificator")
root.geometry("500x400")

# Création des frames pour la sélection des répertoires
frame_directories = tk.Frame(root)
frame_directories.pack(fill=tk.BOTH, expand=True, padx=20, pady=20)

# Répertoire source
source_frame = tk.Frame(frame_directories, width=200, height=50, bd=1, relief=tk.SOLID)
source_frame.pack(side=tk.LEFT, fill=tk.Y, padx=10)
source_frame.pack_propagate(False)

source_label = tk.Label(source_frame, text="Répertoire Source")
source_label.pack(anchor="w")

source_directory = tk.StringVar()
source_entry = tk.Entry(source_frame, textvariable=source_directory, width=50)
source_entry.pack(fill=tk.X)
source_entry.insert(0, "D:\\Test source")

file_counted_sv = tk.StringVar()
file_counted_label = tk.Label(source_frame, textvariable=file_counted_sv)
file_counted_label.pack(anchor="w")

source_button = tk.Button(source_frame, text="Sélectionner", command=select_source_directory)
source_button.pack(pady=5)


# Répertoire de destination
dest_frame = tk.Frame(frame_directories, width=200, height=100, bd=1, relief=tk.SOLID)
dest_frame.pack(side=tk.RIGHT, fill=tk.Y, padx=10)
dest_frame.pack_propagate(False)

dest_label = tk.Label(dest_frame, text="Répertoire de Destination")
dest_label.pack(anchor="w")

destination_directory = tk.StringVar()
dest_entry = tk.Entry(dest_frame, textvariable=destination_directory, width=50)
dest_entry.pack(fill=tk.X)
dest_entry.insert(0, "D:\\Test dest")

directory_reproduction_var = tk.BooleanVar()
directory_reproduction_check_button = tk.Checkbutton(dest_frame, text="Reproduire l'arborescence", variable=directory_reproduction_var)
directory_reproduction_check_button.pack(anchor="w")

dest_button = tk.Button(dest_frame, text="Sélectionner", command=select_destination_directory)
dest_button.pack(pady=5)

# Barre de progression
progress_frame = tk.Frame(root)
progress_frame.pack(fill=tk.X, padx=20, pady=20)

file_index_sv = tk.StringVar()
file_index_sv.set("")
file_index_label = tk.Label(progress_frame, textvariable=file_index_sv)
file_index_label.pack(anchor="w")

actual_file_sv = tk.StringVar()
actual_file_sv.set("")
actual_file_label = tk.Label(progress_frame, textvariable=actual_file_sv)
actual_file_label.pack(anchor="w")

progress_var = tk.IntVar(value=0)
progress_bar = ttk.Progressbar(progress_frame, variable=progress_var, maximum=100)
progress_bar.pack(fill=tk.X)

# Frame pour les zones de texte de la taille 
size_frame = tk.Frame(root)
size_frame.pack(fill=tk.X, padx=20, pady=20)

validate_number_cmd = root.register(validate_number)

width_label = tk.Label(size_frame, text="Largeur")
width_label.pack(side=tk.LEFT, padx=5)
width_entry = tk.Entry(size_frame, width=15, validate="key", validatecommand=(validate_number_cmd, '%P'))
width_entry.pack(side=tk.LEFT, padx=5)
width_entry.insert(0, "2400") 

height_label = tk.Label(size_frame, text="Longueur")
height_label.pack(side=tk.LEFT, padx=5)
height_entry = tk.Entry(size_frame, width=15, validate="key", validatecommand=(validate_number_cmd, '%P'))
height_entry.pack(side=tk.LEFT, padx=5)
height_entry.insert(0, "1600") 

dpi_label = tk.Label(size_frame, text="PPP")
dpi_label.pack(side=tk.LEFT, padx=5)
dpi_entry = tk.Entry(size_frame, width=15, validate="key", validatecommand=(validate_number_cmd, '%P'))
dpi_entry.pack(side=tk.LEFT, padx=5)
dpi_entry.insert(0, "350") 

# Création des boutons pour démarrer et quitter l'application
button_frame = tk.Frame(root)
button_frame.pack(fill=tk.X, padx=20, pady=20)

start_button = tk.Button(button_frame, text="Démarrer", command=start_progress)
start_button.pack(side=tk.LEFT, padx=5)

exit_button = tk.Button(button_frame, text="Quitter", command=root.quit)
exit_button.pack(side=tk.RIGHT, padx=5)

# Boucle principale de l'application
root.mainloop()
